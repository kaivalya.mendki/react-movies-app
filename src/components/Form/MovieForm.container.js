import { connect } from "react-redux"
import { updateData, submit } from "../../store/form/action"
import MovieForm from './MovieForm'

function mapStateToProps(state) {
    return {
        data: state.form.data,
        success: state.form.response.success,
        errors: state.form.response.errors
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateMovieFormData: (data) => dispatch(updateData(data)),
        addMovie: (movie) => dispatch(submit(movie))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieForm)