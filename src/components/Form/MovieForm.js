import React, { useCallback } from 'react'
import '../../styles/components/MovieForm.css'

export default function MovieForm({ data, errors, updateMovieFormData, addMovie }) {
    const setTitle = (title) => {
        updateMovieFormData({
            ...data, 
            title
        })
    }
    
    const setYear = (year) => {
        updateMovieFormData({
            ...data, 
            year
        })
    }
    
    const setGenre = (genre) => {
        updateMovieFormData({
            ...data, 
            genre
        })
    }
    
    const setRating = (rating) => {
        updateMovieFormData({
            ...data, 
            rating
        })
    }

    const submitHandler = useCallback((event) => {
        event.preventDefault();

        addMovie({
            ...data,
            type: "movie"
        });

    }, [data, addMovie]);

    return (
        <form id="movie-form">
            <div className="form-group">
                <div className="row">
                    <div className="col-6">
                        <div className="form-input">
                            <label htmlFor="title" className="form-label">Title
                                <span className="error">
                                    {errors.title ? errors.title[0]: ""}
                                </span>
                            </label>
                            <input type="text" id="title" name="title" value={data.title} placeholder="Movie Title..."
                                onChange={event => setTitle(event.target.value)} 
                                // onBlur={updateDataToStore}
                            />
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="form-input">
                            <label htmlFor="year" className="form-label">Year of Release
                                <span className="error">
                                    {errors.year ? errors.year[0]: ""}
                                </span>
                            </label>
                            <input type="number" id="year" name="year" value={data.year} min="1900" placeholder="Year of movie release..."
                                onChange={event => setYear(event.target.value)}
                            />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-6">
                        <div className="form-input">
                            <label htmlFor="genre" className="form-label">Genre
                                <span className="error">
                                    {errors.genre ? errors.genre[0]: ""}
                                </span>
                            </label>
                            <input type="text" id="genre" name="genre" value={data.genre} placeholder="Movie Genre..."
                                onChange={event => setGenre(event.target.value)}
                            />
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="form-input">
                            <label htmlFor="rating" className="form-label">Rating
                                <span className="error">
                                    {errors.rating ? errors.rating[0]: ""}
                                </span>
                            </label>
                            <input type="number" id="rating" name="rating" value={data.rating} min="0.0" max="10.0" step="0.1" placeholder="Movie rating..."
                                onChange={event => setRating(event.target.value)}
                            />
                        </div>
                    </div>
                </div>

                <button type="submit" onClick={submitHandler} className="btn btn-success">Submit</button>
            </div>
        </form>
    );
}