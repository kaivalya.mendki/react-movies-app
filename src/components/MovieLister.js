import React from 'react'

import '../styles/components/MovieList.css'
import MovieCard from './MovieCard'

export default function MovieLister({ movies, onLikeHandler }) {

    return (
        <div className="movies">
            {
                movies.map((movie) => {
                    const setFavorite = () => onLikeHandler(movie);
                    return <MovieCard key={movie.id} movie={movie} onLike={setFavorite}></MovieCard>
                })
            }
        </div>
    );
} 