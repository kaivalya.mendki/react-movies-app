import React from 'react'

import '../styles/components/Modal.css'

export default function Modal({ modalTitle, modalContent, modalVisibility, toggleModalVisibility }) {
    
    return (
        <div className={ modalVisibility ? "modal" : "hide" }>
            <div className="modal-content">
                <div className="modal-header">
                    <span role="button" className="close" onClick={ toggleModalVisibility }>&times;</span>
                    <div className="modal-title">{modalTitle}</div>
                </div>
                <div className="modal-body">
                    {modalContent}
                </div>
            </div>
        </div>
    );
}