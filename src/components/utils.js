import uuid from 'react-uuid'

function generateId() {
    return uuid();
}

export const utils = {

    transformMovies: function (movies) {
        return movies.map(movie => {
            return {
                id: generateId(),
                title: movie.Title,
                year: movie.Year,
                type: movie.Type,
                rating: "9.5",
                genre: "Adventure. Sci-fi",
                img: "",
            }
        });
    },

    readFavoritesFromLocal: () => {
        const LOCAL_FAVORITES_KEY = "FAVORITES";
        return JSON.parse(localStorage.getItem(LOCAL_FAVORITES_KEY) || "{}");
    },

    writeFavoriteToLocal: (favorites) => {
        const LOCAL_FAVORITES_KEY = "FAVORITES";
        localStorage.setItem(LOCAL_FAVORITES_KEY, JSON.stringify(favorites));
    },

    readFromLocal: (key) => {
        return JSON.parse(localStorage.getItem(key) || "{}")
    },

    writeToLocal: (key, data) => {
        localStorage.JSON.parse(key, JSON.stringify(data));
    }
}