import React from 'react'

import '../styles/components/Loader.css'

export default function Loader() {
    return (
        <div className="loader" data-testid="loader">
            <div className="circle-1"></div>
            <div className="circle-2"></div>
            <div className="circle-3"></div>
        </div>
    );
}