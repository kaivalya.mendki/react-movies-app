import React from 'react'
import { Link } from 'react-router-dom'

import '../styles/components/header.css';

export default function Header({ onSelectHome, onSelectFavorites, toggleModalVisibility }) {
	return (
		<header aria-label="header">
			<div className="logo" title="logo">
				ThoughtWorks &copy;
            </div>
			<nav className="topnav">
				<ul>
					<li onClick={onSelectHome}>
						<Link to="/">Home</Link>
					</li>
					<li onClick={onSelectFavorites}>
						<Link to="/">Favorites</Link>
					</li>
					<li onClick={ toggleModalVisibility }>
						<Link to=""><i className="fa fa-plus"></i> Add Movie</Link>
					</li>
				</ul>
			</nav>
		</header>
	);
}
