import React, { useState } from 'react'

import '../styles/components/movie-card.css';

export default function MovieCard(props) {
    const [liked, setLiked] = useState(false);

    const toggleLike = () => {
        setLiked(!liked);
        props.onLike();
    }

    return (
        <div className="card" title={props.movie.title}>
            <div className="card-header">
                <img className="card-img" src="https://image.tmdb.org/t/p/w500/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg" alt="Movie Card" />
            </div>
            <div className="card-body">
                <h4 className="card-title">{props.movie.title}</h4>
                <small>{props.movie.year}</small>
                <div className="card-data">
                    <div className="metadata">
                        <small className="rating"><i className="fa fa-star"></i> {props.movie.rating}/10</small>
                        <small className="genre">{props.movie.genre}</small>
                    </div>
                    <div className="like-btn" role="button" onClick={toggleLike}>
                        <i title={liked ? "Dislike Movie" : "Like Movie"} className={liked ? "fa fa-heart" : "fa fa-heart-o"}></i>
                    </div>
                    {/* <p className="card-text">A team of explorers travel through wormhole in space in an attempt to ensure humanity's survival.</p> */}
                </div>
            </div>
        </div>
    );
}