import React from 'react'
import '../styles/components/Banner.css'

export default function Banner() {
    return(
		<img className="hero-img" 
            src="https://www.technobuffalo.com/sites/technobuffalo.com/files/styles/xlarge/public/wp/2017/08/netflix-library.jpg" 
            alt="Banner" />
    );
}