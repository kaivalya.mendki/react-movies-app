export const movies = [
	{
		id: 1,
		title: "Interstellar",
		year : "2006",
		type : "movie",
		rating: "9.5",
		genre: "Adventure. Sci-fi",
		img: "https://image.tmdb.org/t/p/w500/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg"
	}, {
		id: 2,
		title: "The Last Airbender",
		year : "2010",
		type : "movie",
		rating: "9.5",
		genre: "Adventure. Sci-fi",
		img: "https://static.rogerebert.com/uploads/movie/movie_poster/the-last-airbender-2010/large_5kO6hVZrtBZ98VfpgHvwivjXgMg.jpg"
	}, {
		id: 3,
		title: "Star Wars: The Last Jedi",
		year : "2017",
		type : "movie",
		rating: "9.5",
		genre: "Adventure. Sci-fi",
		img: "https://m.media-amazon.com/images/M/MV5BMjQ1MzcxNjg4N15BMl5BanBnXkFtZTgwNzgwMjY4MzI@._V1_.jpg"
	}, {
		id: 4,
		title: "The Last King of Scotland",
		year : "2006",
		type : "movie",
		rating: "9.5",
		genre: "Adventure. Sci-fi",
		img: "https://bestsimilar.com/img/movie/thumb/fc/17011.jpg"
	}, {
		id: 5,
		title: "The Last Stand",
		year : "2013",
		type : "movie",
		rating: "9.5",
		genre: "Adventure. Sci-fi",
		img: "https://2.bp.blogspot.com/-_m4QFaMkAP0/UPQZWMw0u8I/AAAAAAAAFmI/RPXGjzu3nmQ/s1600/Last+Stand+2013+film+movie+poster+large.jpg"
	}, {
		id: 6,
		title: "X-Men: The Last Stand",
		year : "2006",
		type : "movie",
		rating: "9.5",
		genre: "Adventure. Sci-fi",
		img: "https://m.media-amazon.com/images/M/MV5BNDBhNDJiMWEtOTg4Yi00NTYzLWEzOGMtMjNmNjAxNTBlMzY3XkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_.jpg"
	}, {
		id: 7,
		title: "The Last Airbender",
		year : "2010",
		type : "movie",
		rating: "9.5",
		genre: "Adventure. Sci-fi",
		img: "https://static.rogerebert.com/uploads/movie/movie_poster/the-last-airbender-2010/large_5kO6hVZrtBZ98VfpgHvwivjXgMg.jpg"
	}, {
		id: 8,
		title: "Star Wars: The Last Jedi",
		year : "2017",
		type : "movie",
		rating: "9.5",
		genre: "Adventure. Sci-fi",
		img: "https://m.media-amazon.com/images/M/MV5BMjQ1MzcxNjg4N15BMl5BanBnXkFtZTgwNzgwMjY4MzI@._V1_.jpg"
	}, {
		id: 9,
		title: "The Last King of Scotland",
		year : "2006",
		type : "movie",
		rating: "9.5",
		genre: "Adventure. Sci-fi",
		img: "https://bestsimilar.com/img/movie/thumb/fc/17011.jpg"
	}, {
		id: 10,
		title: "The Last Stand",
		year : "2013",
		type : "movie",
		rating: "9.5",
		genre: "Adventure. Sci-fi",
		img: "https://2.bp.blogspot.com/-_m4QFaMkAP0/UPQZWMw0u8I/AAAAAAAAFmI/RPXGjzu3nmQ/s1600/Last+Stand+2013+film+movie+poster+large.jpg"
	},
]
