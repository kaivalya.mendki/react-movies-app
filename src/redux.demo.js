const Redux = require('redux');
const uuidv4 = require('uuid/v4');

let counter = 0;
const idGenerator = () => counter++;

const removeFromList = (list, id) => {
    return list.filter((item) => item.id !== id)
}

const markTaskCompleted = (list, id) => [
    // return list.filter((task) => )
]

const state = {
    todos: [
        {
            text: "Task 1",
            id: '1',
            completed: false
        },
        {
            text: "Task 2",
            id: '2',
            completed: false
        }
    ]
}

const ACTION_TYPE = {
    ADD_TODO: "ADD_TODO",
    DELETE_TODO: "DELETE_TODO",
    MARK_COMPLETD: "MARK_COMPLETED"
}

const addToDo = (text) => {
    return {
        type: ACTION_TYPE.ADD_TODO,
        payload: {
            text
        },
        perform: function(state) {
            return {
                ...state,
                todos: [
                    ...state.todos,
                    {
                        id: idGenerator(),
                        text: text,
                        completed: false
                    }
                ]
            }
        }
    }
}

const deleteFromToDo = (id) => {
    return {
        type: ACTION_TYPE.DELETE_TODO,
        payload: {
            id
        },
        perform: function(state) {
            return {
                ...state,
                todos: removeFromList(state.todos, id)
            }
        }
    }
}

const markCompleted = (id) => {
    return {
        type: ACTION_TYPE.MARK_COMPLETD,
        payload: {
            id
        },
        perform: function(state) {
            return {
                ...state,
                todos: markTaskCompleted(state.todos, id)
            }
        }
    }
}

const firstAddAction = addToDo("Task 1");
const secondAddAction = addToDo("Task 2");

const firstDeleteAction = deleteFromToDo(1);

const reducer = (state , action) => {
    if(action.perform) {
        return action.perform(state);
    }
    else
        return state
}

const initialState = { todos: []};
const store = Redux.createStore(reducer, initialState);

store.subscribe(() => {
    console.log(store.getState());
});

store.dispatch(firstAddAction);
store.dispatch(secondAddAction);

store.dispatch(firstDeleteAction);