export const getAll = (url) => {
    return fetch(url).then(response => {
        if(!response.ok) {
            const error = {
                success: false, 
                status: response.status
            }
            throw error;
        } 
        return response.json();
    });
}

export const post = (movie) => {
    return Promise.resolve(true);
}