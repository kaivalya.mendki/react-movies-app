import { getAll } from "./MovieApi";
import MovieValidator from '../utility/MovieValidator'
import { utils } from '../components/utils'

export const movieApi = {

    getMovies: () => {
        const url = "https://tw-frontenders.firebaseio.com/movies.json";
        return getAll(url).then((response) => {
            return utils.transformMovies(response);
        }, (error) => {
            return Promise.reject(error);
        });
    },

    addMovie: (movie) => {
        // return post(movie);
        const result = MovieValidator().validate(movie);
        if(result.success)
            return Promise.resolve(result);
        else{
            return Promise.reject(result);
        }
    }
}