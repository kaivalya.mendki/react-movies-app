import FormValidator from "./FormValidator";

const emptyCheck = v => v && v.trim().length > 0;

export default function MovieValidator() {
    const validator = new FormValidator();
    validator
        .addRule("title", emptyCheck, "Title cannot be empty")
        .addRule("year", emptyCheck, "Year of release cannot be empty")
        .addRule("rating", emptyCheck, "Rating cannot be empty")
        .addRule("genre", emptyCheck, "Genre cannot be empty");

    return validator;
}
