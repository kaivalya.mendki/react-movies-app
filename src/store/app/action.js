export const ACTION_TYPE = {
    INIT: 'INIT',
    FAIL: 'FAIL',
    SET_APP_DATA: 'SET_APP_DATA',
    ADD_FAVORITES: 'ADD_FAVORITES',
    TOGGLE_MOVIE_MODAL: 'TOGGLE_MOVIE_MODAL',
    SET_LOADING: 'SET_LOADING'
}

export function initAction() {
    return {
        type: ACTION_TYPE.INIT
    };
}

export function fail() {
    return {
        type: ACTION_TYPE.FAIL
    }
}

export function setAppData(movies) {
    return {
        type: ACTION_TYPE.SET_APP_DATA,
        payload: {
            movies
        }
    }
}

export function addFavorites(favorites) {
    return {
        type: ACTION_TYPE.ADD_FAVORITES,
        payload: {
            favorites
        }
    }
}

export function toggleMovieModalVisibility() {
    return {
        type: ACTION_TYPE.TOGGLE_MOVIE_MODAL
    }
}

export function setLoading(isLoading) {
    return {
        type: ACTION_TYPE.SET_LOADING,
        payload: {
            isLoading
        }
    }
}
