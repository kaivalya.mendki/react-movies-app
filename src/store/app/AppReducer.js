import { loop, Cmd } from 'redux-loop';
import { ACTION_TYPE, setAppData, fail, setLoading } from './action'
import { movieApi } from '../../api/ApiCaller';

const initialState = {
    movies: {},
    favorites: {},
    modalVisibility: false,
    isLoading: false
}

const initializeMovieMapping = (movies) => {
    const movieMapping = movies.reduce((acc, movie) => {
        acc[movie.id] = movie;
        return acc;
    }, {});
    return movieMapping;
}

const initializeFavoritesMapping = (movies) => {
    const favoritesMapping = movies.reduce((acc, movie) => {
        acc[movie.id] = false;
        return acc;
    }, {});
    return favoritesMapping;
}

export const fetchMovies = () => {
    return movieApi.getMovies();
}


export function AppReducer(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPE.INIT:
            return loop({ ...state, isLoading: true },
                Cmd.run(fetchMovies, {
                    successActionCreator: setAppData,
                    failActionCreator: fail,
                })
            );

        case ACTION_TYPE.SET_APP_DATA:
            const movies = initializeMovieMapping(action.payload.movies);
            const favorites = initializeFavoritesMapping(action.payload.movies);
            return loop({
                    ...state,
                    movies: movies,
                    favorites: favorites
                },
                Cmd.action(setLoading(false))
            );

        case ACTION_TYPE.ADD_FAVORITES:
            return {
                ...state,
                favorites: action.payload.favorites
            }

        case ACTION_TYPE.TOGGLE_MOVIE_MODAL:
            return {
                ...state,
                modalVisibility: !state.modalVisibility
            }

        case ACTION_TYPE.SET_LOADING:
            return {
                ...state,
                isLoading: action.payload.isLoading
            }

        case ACTION_TYPE.FAIL:
            console.log("failed");
            return state

        default:
            return state
    }
}