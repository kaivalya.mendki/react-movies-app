import { combineReducers } from 'redux-loop'
import { AppReducer } from '../store/app/AppReducer'
import { FormReducer } from './form/FormReducer'

export const RootReducer = combineReducers({
    app: AppReducer,
    form: FormReducer
})