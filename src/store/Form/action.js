export const FORM_ACTION_TYPE = {
    UPDATE_DATA: 'UPDATE_DATA',
    ON_SUBMIT: 'ON_SUBMIT',
    ON_SUCCESS: 'ON_SUCCESS',
    ON_ERROR: 'ON_ERROR',
    ON_RESET: 'ON_RESET',
}

export function updateData(data) {
    return {
        type: FORM_ACTION_TYPE.UPDATE_DATA,
        payload: {
            data
        }
    }
}

export function submit(movie) {
    return {
        type: FORM_ACTION_TYPE.ON_SUBMIT,
        payload: {
            movie
        }
    }
}


export function success(response) {
    return {
        type: FORM_ACTION_TYPE.ON_SUCCESS,
        payload: {
            response
        }
    }
}

export function error(response) {
    return {
        type: FORM_ACTION_TYPE.ON_ERROR,
        payload: {
            response
        }
    }   
}

export function reset() {
    return {
        type: FORM_ACTION_TYPE.ON_RESET,
    }
}