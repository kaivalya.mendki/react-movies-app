import { loop, Cmd } from 'redux-loop';
import { FORM_ACTION_TYPE, success, error, reset } from './action'
import { setLoading, toggleMovieModalVisibility } from '../app/action';
import { movieApi } from '../../api/ApiCaller';

import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const FormSwal = withReactContent(Swal)

const initialState = {
    data: {
        title: "",
        year: "",
        genre: "",
        rating: "",
    },
    response: { success: true, errors: {} }
}

const showSuccessAlert = (title, text) => {
    FormSwal.fire({
        title: title,
        text: text,
        icon: 'success'
      })
}

export function FormReducer(state = initialState, action) {
    switch(action.type){
        case FORM_ACTION_TYPE.UPDATE_DATA:
            return {
                ...state,
                data: action.payload.data
            }

        case FORM_ACTION_TYPE.ON_SUBMIT:
            return loop ({
                    ...state,
                },
                Cmd.list([
                    Cmd.action(setLoading(true)),
                    Cmd.run(movieApi.addMovie, {
                        successActionCreator: success,
                        failActionCreator: error,
                        args: [action.payload.movie]
                    })
                ])
            );
            
        case FORM_ACTION_TYPE.ON_SUCCESS:
            return loop ({
                    ...state,
                    response: action.payload.response
                },
                Cmd.list([
                    Cmd.action(reset()),
                    Cmd.run(showSuccessAlert, {
                        args: ["Good Job!", "New Movie has been successfully added!"]
                    }),
                    Cmd.action(toggleMovieModalVisibility()),
                    Cmd.action(setLoading(false))
                ])
            );

        case FORM_ACTION_TYPE.ON_ERROR:
            return loop ({
                ...state,
                response: action.payload.response
            },
            Cmd.action(setLoading(false))
        );

        case FORM_ACTION_TYPE.ON_RESET:
            return {
                ...state,
                ...initialState
            }
        
        default:
            return state
    }
}
