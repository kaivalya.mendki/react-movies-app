import React from 'react'

import { render, screen } from "@testing-library/react";
import { movies } from '../../sample-data'
import MovieLister from '../../components/MovieLister';


describe("MovieLister Tests", () => {
    it("should render all the movies from movie list", () => {
        const setFavorite = jest.fn();
        render(<MovieLister movies={movies} onLikeHandler={setFavorite} />);
        
        movies.forEach(movie => {
            expect(screen.getAllByTitle(movie.title)).toBeTruthy();
        });
    });

});
