import React from 'react'
import { render, screen } from '@testing-library/react';
import Loader from '../../components/Loader';

describe("Loader Tests", () => {
    it("should render loader on the screen", () => {
        render(<Loader />);

        expect(screen.getByTestId("loader")).toBeInTheDocument();
    });
});