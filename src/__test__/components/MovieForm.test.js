import React from 'react';

import { render, screen } from '@testing-library/react';
import MovieForm from '../../components/MovieForm';
import userEvent from "@testing-library/user-event";
import { movieApi } from '../../api/ApiCaller';

describe('Movie Form Tests', () => {
    beforeEach(() => {
        render(<MovieForm />);
    });

    describe("Form-field Title", () => {
        it('should render form-field Title of type text on the screen', () => {
            const nameField = screen.getByLabelText("Title");
            expect(nameField).toBeInTheDocument();
            expect(nameField).toHaveAttribute("type", expect.stringMatching("text"));
        });

        it("should set value as given by user input", () => {
            const nameField = screen.getByLabelText("Title");

            userEvent.type(nameField, "--name--");

            expect(nameField).toHaveValue("--name--");
        });
    });

    describe('Form-field Year of Release', () => {
        it('should render form-field Year of release of type number on the screen', () => {
            const yearField = screen.getByLabelText("Year of Release");

            expect(yearField).toBeInTheDocument();
            expect(yearField).toHaveAttribute("type", expect.stringMatching("number"));
        });

        it("should have min value for input 1900", () => {
            const yearField = screen.getByLabelText("Year of Release");

            expect(yearField).toHaveAttribute("min", expect.stringMatching("1900"));
        });

        it("should set value as given by user input", () => {
            const yearField = screen.getByLabelText("Year of Release");

            userEvent.type(yearField, "2019");

            expect(yearField).toHaveValue(2019);
        });
    });

    describe('Form-field Rating', () => {
        it("should render form-field Rating of type number on the screen", () => {
            const ratingField = screen.getByLabelText("Rating");

            expect(ratingField).toBeInTheDocument();
            expect(ratingField).toHaveAttribute("type", expect.stringMatching("number"));
        });

        it("should have min-value as 0.0, max-value as 10.0 and step-value as 0.1 for the input", () => {
            const ratingField = screen.getByLabelText("Rating");

            expect(ratingField).toHaveAttribute("min", expect.stringMatching("0.0"));
            expect(ratingField).toHaveAttribute("max", expect.stringMatching("10.0"));
            expect(ratingField).toHaveAttribute("step", expect.stringMatching("0.1"));
        });

        it("should set value as given by user input", () => {
            const ratingField = screen.getByLabelText("Rating");

            userEvent.type(ratingField, "8.7");

            expect(ratingField).toHaveValue(8.7);
        });
    });

    describe("Form-field Genre", () => {
        it("should render form-field Genre of type text on the screen", () => {
            const genreField = screen.getByLabelText("Genre");

            expect(genreField).toBeInTheDocument();
            expect(genreField).toHaveAttribute("type", expect.stringMatching("text"));
        });

        it("should set value as given by user input", () => {
            const genreField = screen.getByLabelText("Genre");

            userEvent.type(genreField, "Thriller");

            expect(genreField).toHaveValue("Thriller");
        });
    });

    describe("Submit Button", () => {
        it("should render submit button on the screen", () => {
            const submitBtn = screen.getByText("Submit");

            expect(submitBtn).toBeInTheDocument();
            expect(submitBtn).toHaveAttribute("type", expect.stringMatching("submit"));
        });

        it("should call api for add movie on submit", () => {
            const submitBtn = screen.getByText("Submit");
            movieApi.addMovie = jest.fn();
            movieApi.addMovie.mockResolvedValueOnce({});

            userEvent.click(submitBtn);

            expect(movieApi.addMovie).toBeCalled();
        });

        it("should render errors on the screen on unsuccessful Add movie attempt", async () => {
            const submitBtn = screen.getByText("Submit");
            movieApi.addMovie = jest.fn();
            movieApi.addMovie.mockRejectedValueOnce({
                success: false,
                errors: {
                    title: ['Title cannot be empty'],
                }
            });

            userEvent.click(submitBtn);

            expect(await screen.findByText("Title cannot be empty")).toBeInTheDocument();
        });
    });
});
