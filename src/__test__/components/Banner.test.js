import React from 'react'
import { render, screen } from "@testing-library/react";

import Banner from '../../components/Banner'

describe("Banner Tests", () => {
    
    it("should render hero banner image in the DOM", () => {
        render(<Banner />);
        
        expect(screen.getByAltText("Banner")).toBeInTheDocument();
    });
});