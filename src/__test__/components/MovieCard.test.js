import React from "react";

import MovieCard from "../../components/MovieCard";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe("Movie Card test", () => {
    const movie = {
        id: 1,
        title: "X-Men: The Last Stand",
        year: "2006",
        type: "movie",
        rating: "9.5",
        genre: "Adventure. Sci-fi"
    };

    it("should render movie with the given title", () => {
        render(<MovieCard key={movie.id} movie={movie}></MovieCard>);

        expect(screen.getByText(movie.title)).toBeTruthy();
    });

    it("should render movie with the given genre", () => {
        render(<MovieCard key={movie.id} movie={movie}></MovieCard>);

        expect(screen.getByText(movie.genre)).toBeTruthy();
    });

    it("should render movie with the given rating", () => {
        render(<MovieCard key={movie.id} movie={movie}></MovieCard>);

        expect(screen.getByText(movie.rating + "/10")).toBeTruthy();
    });

    it("should render button to like the movie if it is unliked", () => {
        render(<MovieCard key={movie.id} movie={movie}></MovieCard>);

        expect(screen.getByTitle("Like Movie")).toBeTruthy();
    });

    it("should render button to dislike the movie if it is liked", () => {
        const onLikeHandler = jest.fn();

        render(
            <MovieCard
                key={movie.id}
                movie={movie}
                onLike={onLikeHandler}
            ></MovieCard>
        );
        userEvent.click(screen.getByTitle("Like Movie"));

        expect(screen.getByTitle("Dislike Movie")).toBeTruthy();
    });
});
