import React from 'react'
import { render, screen } from '@testing-library/react';
import Header from '../../components/Header';
import { MemoryRouter } from 'react-router-dom';

describe("Header Test", () => {

    beforeEach(() => {
        render(
            <MemoryRouter initialEntries={['/']}>
                 <Header/>
            </MemoryRouter>
        );
    });

    it("should render logo on the header", () => {
        expect(screen.getByTitle("logo")).toBeInTheDocument();
    });

    it("should render link for Home on navigation bar in header", () => {
        expect(screen.getByText("Home")).toBeInTheDocument();
    });

    it("should render link for Favorites on navigation bar in header", () => {
        expect(screen.getByText("Favorites")).toBeInTheDocument();
    });

    it("should render link for Adding new movie on navigation bar in header", () => {
        expect(screen.getByText("Add Movie")).toBeInTheDocument();
    });
});