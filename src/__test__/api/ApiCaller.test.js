import React from 'react'
import * as MovieApi from '../../api/MovieApi';
import { movieApi } from '../../api/ApiCaller';

describe("API Call Tests", () => {
    jest.mock('../../api/MovieApi');

    it("should return all errors for Add movie on unsuccessful resolve", () => {
        expect(movieApi.addMovie({})).resolves.toEqual({
            success: false,
            errors: {
                title: [ 'Title cannot be empty' ],
                year: [ 'Year of release cannot be empty' ],
                rating: [ 'Rating cannot be empty' ],
                genre: [ 'Genre cannot be empty' ]
            }
        });
    });

    it("should return all errors for Add movie on unsuccessful resolve", () => {
        const movie = {
            title: '--title--',
            year: '2020',
            genre: '--genre--',
            rating: '9.5',
            type: 'movie'
        }
        expect(movieApi.addMovie(movie)).resolves.toEqual({
            success: true,
            errors: {}
        });
    });
});