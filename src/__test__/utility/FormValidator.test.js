import FormValidator from '../../utility/FormValidator'

const emptyCheck = v => v && v.trim().length > 0;
const lengthCheck = v => v.length > 2;

describe("FormValidator addRule", () => {
    it("should add new rule to the form validator", () => {
        const validator = new FormValidator();

        const expectedRules = {}
        expectedRules["name"] = {
            checkers: [{
                method: emptyCheck,
                errorMsg: "Name cannot be empty"
            }]
        };

        validator.addRule("name", emptyCheck, "Name cannot be empty");

        expect(validator.rules).toEqual(expectedRules);
    });

    it("should add rules one after one in chaining", () => {
        const validator = new FormValidator();

        const expectedRules = {}
        expectedRules["name"] = {
            checkers: [{
                method: emptyCheck,
                errorMsg: "Name cannot be empty"
            }]
        };
        expectedRules["email"] = {
            checkers: [{
                method: emptyCheck,
                errorMsg: "Email cannot be empty"
            }]
        };

        validator
            .addRule("name", emptyCheck, "Name cannot be empty")
            .addRule("email", emptyCheck, "Email cannot be empty");

        expect(validator.rules).toEqual(expectedRules);
    });

    it("should add multiple checkers for same form field", () => {
        const validator = new FormValidator();

        const expectedRules = {}
        expectedRules["name"] = {
            checkers: [{
                method: emptyCheck,
                errorMsg: "Name cannot be empty"
            },{
                method: lengthCheck,
                errorMsg: "Name should be more than 2 characters"
            }]
        };
        expectedRules["email"] = {
            checkers: [{
                method: emptyCheck,
                errorMsg: "Email cannot be empty"
            }]
        };

        validator
            .addRule("name", emptyCheck, "Name cannot be empty")
            .addRule("name", lengthCheck, "Name should be more than 2 characters")
            .addRule("email", emptyCheck, "Email cannot be empty");

        expect(validator.rules).toEqual(expectedRules);
    });

   it("should add rules one after one in chaining for specific field", () => {
       const validator = new FormValidator();

       const expectedRules = {}
       expectedRules["name"] = {
           checkers: [{
                method: emptyCheck,
                errorMsg: "Name cannot be empty"
            }, {
                method: lengthCheck,
                errorMsg: "Name should be more than 2 characters"
            }]
       };

       validator
           .forField("name")
           .addRule(emptyCheck, "Name cannot be empty")
           .addRule(lengthCheck, "Name should be more than 2 characters");

       expect(validator.rules).toEqual(expectedRules);
   });

});

describe("FormValidator validate", () => {

    it("should success on validation of all the checkers for single field", () => {
        const validator = new FormValidator();
        validator
            .addRule("name", emptyCheck, "Name cannot be empty")
            .addRule("name", lengthCheck, "Name should be more than 2 characters");

        const expectedResult = {
            success: true,
            errors: {}
        };

        expect(validator.validate({
            name: "::name::"
        })).toEqual(expectedResult)
    });

    it("should fail and return list of errors on validation of all the checkers for single field", () => {
        const validator = new FormValidator();
        validator
            .addRule("name", emptyCheck, "Name cannot be empty")
            .addRule("name", lengthCheck, "Name should be more than 2 characters");

        const expectedResult = {
            success: false,
            errors: {
                name: ["Name cannot be empty", "Name should be more than 2 characters"]
            }
        };

        expect(validator.validate({
            name: ""
        })).toEqual(expectedResult)
    });

    it("should fail and return lists of errors with their related fields on validation of all the checkers for multiple fields", () => {
        const validator = new FormValidator();
        validator
            .addRule("name", emptyCheck, "Name cannot be empty")
            .addRule("name", lengthCheck, "Name should be more than 2 characters")
            .addRule("email", emptyCheck, "Email cannot be empty");

        const expectedResult = {
            success: false,
            errors: {
                name: [ 'Name should be more than 2 characters' ],
                email: [ 'Email cannot be empty' ]
            }
        }

        expect(validator.validate({
            name: "na",
            email: ""
        })).toEqual(expectedResult)
    });
});
