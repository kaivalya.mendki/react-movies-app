import { Cmd, loop } from 'redux-loop';
import { AppReducer, fetchMovies } from '../../../store/app/AppReducer';
import { addMovies, fail, initAction } from '../../../store/app/action'

describe("Home Reducer Tests", () => {
    it('reducer works as expected', () => {
        const state = {
            initStarted: false,
            movies: {},
            favorites: {},
            modalVisibility: false
        }
    
        const result = AppReducer(state, initAction());
    
        expect(result).toEqual(loop(
            { ...state, initStarted: true },
            Cmd.run(fetchMovies, {
                successActionCreator: addMovies,
                failActionCreator: fail,
                args: []
            })
        ));
    });
});