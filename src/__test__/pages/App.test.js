import React from 'react'
import { screen, render } from '@testing-library/react';
import userEvent from "@testing-library/user-event";

import App from '../../pages/App'

describe("App Tests", () => {

    beforeEach(() => {
        render(<App />);
    });

    it("should render Header on the screen", () => {
        expect(screen.getByLabelText("header")).toBeInTheDocument();
    });

    it("should navigate to Home when Home-Link is clicked", () => {
        userEvent.click(screen.getByText("Home"));

        expect(screen.getByTestId("home")).toBeInTheDocument();
    });

});