import React from 'react'
import { render, screen } from '@testing-library/react';
import Home from '../../pages/Home';

describe("Home Tests", () => {

    it("should set list tile as Movies if show favorites is false", () => {
        render(<Home showFavorites={false}/>)

        expect(screen.getByText("Movies")).toBeTruthy();
    });

    it("should set list tile as Favorites if show favorites is true", () => {
        render(<Home showFavorites={true}/>)

        expect(screen.getByText("Favorites")).toBeTruthy();
    });

    it("should render favorites count on the screen", () => {
        render(<Home />);

        expect(screen.getByTitle("Favorites-count")).toBeInTheDocument();
    });

    it("should render all movies on screen", () => {
        expect(true).toBeTruthy();
    });
});