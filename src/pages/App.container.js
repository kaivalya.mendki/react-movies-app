import { connect } from "react-redux"
import Home from "./Home"
import { initAction, addFavorites, toggleMovieModalVisibility, setLoading } from "../store/app/action"
import Modal from "../components/Modal"
import Header from "../components/Header"

function mapStateToProps(state) {
    return {
        movies: state.app.movies,
        favorites: state.app.favorites,
        modalVisibility: state.app.modalVisibility,
        isLoading: state.app.isLoading
    }
}

function mapDispatchToProps(dispatch) {
    return {
        init: () => dispatch(initAction()),
        setFavorites: (favorites) => dispatch(addFavorites(favorites)),
        toggleModalVisibility: () => dispatch(toggleMovieModalVisibility()),
        setLoading: (isLoading) => dispatch(setLoading(isLoading))
    }
}

export const NewHome = connect(mapStateToProps, mapDispatchToProps)(Home)
export const NewModal = connect(mapStateToProps, mapDispatchToProps)(Modal)
export const NewHeader = connect(mapStateToProps, mapDispatchToProps)(Header)