import React, { useState } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import '../styles/app.css'
import MovieForm from '../components/Form/MovieForm.container'
import { setupAppStore } from '../store'
import { Provider } from 'react-redux'
import { NewHome, NewModal, NewHeader } from './App.container'

const store = setupAppStore();

export default function App() {
	const [showFavorites, setshowFavorites] = useState(false);

	const onSelectHome = () => setshowFavorites(false);
	const onSelectFavorites = () => setshowFavorites(true);

	return (
		<Provider store={store}>
			<BrowserRouter>
				<NewHeader onSelectHome={onSelectHome} onSelectFavorites={onSelectFavorites} />
				<main className="container">
					<Switch>
						<Route exact path="/">
							<NewHome showFavorites={showFavorites} />
						</Route>
					</Switch>
					<NewModal modalTitle={"Add New Movie"} modalContent={<MovieForm />} />
				</main>
			</BrowserRouter>
		</Provider>
	);
}

