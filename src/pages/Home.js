import React, { useState, useEffect } from 'react'

import '../styles/components/Home.css'
import Banner from '../components/Banner'
import MovieLister from '../components/MovieLister';

import Loader from '../components/Loader'

export default function Home({ showFavorites, init, movies, favorites, setFavorites, isLoading, setLoading }) {
    const [favoritesCount, setFavoritesCount] = useState(0);

    useEffect(() => {
        init();
    }, [init]);

    // useEffect(() => {
    //     setFavorites(utils.readFavoritesFromLocal());
    // }, []);

    // useEffect(() => {
    //     utils.writeFavoriteToLocal(favorites);
    // }, [favorites, favoritesCount]);

    const onLikeHandler = (movie) => {
        favorites[movie.id] = !favorites[movie.id];
        favorites[movie.id] ? setFavoritesCount(favoritesCount + 1) : setFavoritesCount(favoritesCount - 1);
    }

    const favoriteMovies = () => {
        return Object.keys(favorites)
            .filter((movieId) => favorites[movieId] === true)
            .map(movieId => movies[movieId]);
    }

    const allMovies = () => {
        return Object.values(movies);
    }

    return (
        <div data-testid="home">
            <Banner />
            {
                isLoading ?
                    <Loader /> :
                    <div className="movie-list">
                        <div className="list-title">
                            <h1>{showFavorites ? "Favorites" : "Movies"}</h1>
                        </div>

                        <span className="favorites-count" title="Favorites-count">
                            <i className="fa fa-heart"></i> {favoritesCount}
                        </span>
                        <MovieLister movies={showFavorites ? favoriteMovies() : allMovies()} onLikeHandler={onLikeHandler} />
                    </div>
            }
        </div>
    );
}
